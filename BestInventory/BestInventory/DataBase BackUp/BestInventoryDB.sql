CREATE DATABASE BestInventoryDB

USE BestInventoryDB

CREATE TABLE ITEM(
ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
ITEM_NAME VARCHAR(20),
DETAIL VARCHAR(50),
QUT INT,
COST MONEY,
PRICE MONEY,
REORDER INT,
CID INT,
FOREIGN KEY (CID) REFERENCES CATEGORY(CID)
);

INSERT INTO ITEM(ITEM_NAME,DETAIL,QUT,COST,PRICE,REORDER,CID) VALUES('agfv','ahtn fsbzz',6,650,770,10,2),
('Sectional Sofa','Multi-Piece',10,30000,35000,5,1),
('Chesterfield','Quilted Sofa',12,25000,30000,5,1),
('Futon','Sofa/bed',13,45000,50000,5,1),
('Divan','Without Back',9,20000,30000,5,1),
('Love Seat Sofas','Only Two Pepole',7,25000,32000,3,1),

('Toeni Six Seater','Dining Set',7,43500,50000,5,2),
('Calypso','Dining Set',8,47500,50000,11,2),
('Bergamo','Oak Finish',9,50000,60000,3,2),
('Tremlett','Six Seater',13,29000,35000,5,2),
('Peterhouse','Oak Finish',13,35000,43000,5,2),

('Passion','Brown Color',15,7000,10000,10,3),
('High Back','Stealer',12,6000,7500,5,3),
('Gardern','Wood Color',20,1000,2000,5,3),
('Arm Chair','Back Cushion',10,3500,4000,5,3),
('Vita Rocker','White Color',13,8000,10000,3,3),

('Metalic','Queen Size',9,5500,6000,5,4),
('Crescent','Spacewood',11,20000,25000,3,4),
('Metalic','FurnitureKraft',7,5000,6000,3,4),
('Hideki','Mintwud',6,12000,15000,3,4),
('Marion','Red bred',7,17000,22000,3,4),

('Bar Stool','Mintwud',9,4000,5000,10,5),
('Bar Stool','Red Color',11,4500,5000,5,5),
('Plastic Stool','Blue',8,500,700,5,5),
('Hart Shape','Sarthak Designs',7,15000,17000,7,5),
('EllaSmall','Siwa',22,5000,6500,3,5),

('Rall Away','Springtek',7,10000,15000,3,6),
('Beat Futon','Arpa',9,18000,22000,5,6),
('Doubl Futon','Orange',7,15000,17000,3,6),
('Ultra Care','Spring Teck',15,15000,18000,5,6),
('Foldable','Blue Color',18,6000,7500,5,6);

SELECT * FROM ITEM;


CREATE TABLE CATEGORY(
CID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
CATEGORY_NAME VARCHAR(20) NOT NULL 
);

INSERT INTO CATEGORY VALUES('SOFA'),('TABLE'),('CHAIR'),('BED'),('STOOL'),('MATTRESS');
SELECT * FROM CATEGORY;


CREATE TABLE USERS(
ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
FNAME VARCHAR(20),
LNAME VARCHAR(20),
EMAIL VARCHAR(30) UNIQUE,
PASS VARCHAR(20),
UTYPE VARCHAR(20) DEFAULT 'USER' 
);

INSERT INTO USERS(FNAME,LNAME,EMAIL,PASS,UTYPE) VALUES('Ashok','Kamara','ashokkumara20@gmail.com','ashok','ADMIN'),
('Nirmana','Sankalpa','nirmana666@gmail.com','nirmana','ADMIN'),
('Chrishan','Perera','pererachrishan729@gmail.com','chrishan','USER'),
('Duwane','Wanigasinhe','duwane@eshcolgroup.com','duwane','USER'),
('Anjana','Silva','anjana13874@gmail.com','anjana','USER');

SELECT * FROM USERS;

