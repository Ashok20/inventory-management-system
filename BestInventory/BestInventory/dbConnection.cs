﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BestInventory
{
    public class dbConnection
    {
        SqlConnection con;
        SqlCommand cmd = new SqlCommand();
        public dbConnection()
        {
            string cs = ConfigurationManager.ConnectionStrings["BestInventoryDB"].ConnectionString;
            con = new SqlConnection(cs);
        }        
        public SqlDataAdapter getDataFromDB(string sql)
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            con.Close();
            return da;    
        }

        public void addDataToDB(String sql)
        {
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public string getSingleValue(String sql)
        {
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            string value = (string)cmd.ExecuteScalar();
            con.Close();
            return value;
        }
    }
}