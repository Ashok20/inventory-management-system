﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/MasterWeb.master" AutoEventWireup="true" CodeBehind="inventory.aspx.cs" Inherits="BestInventory.pages.inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="nestedTitle" runat="server">
    Inventory
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Body1_Heading" runat="server">
    Inventory
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="Body2_Rest" runat="server">

    <div class="row">
        <%-- <div class="col-lg-12">--%>
        <div class="container col-sm-12 col-lg-12">

            <br />

            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><br /></div>

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="BIDB" GridLines="None" CssClass="table">
                    <Columns>
                        <asp:BoundField DataField="ITEM_NAME" HeaderText="Item" SortExpression="ITEM_NAME" />
                        <asp:BoundField DataField="DETAIL" HeaderText="Description" SortExpression="DETAIL" />
                        <asp:BoundField DataField="QUT" HeaderText="Qut" SortExpression="QUT" />
                        <asp:BoundField DataField="COST" HeaderText="Cost" SortExpression="COST" />
                        <asp:BoundField DataField="PRICE" HeaderText="Price" SortExpression="PRICE" />
                        <asp:BoundField DataField="REORDER" HeaderText="ReOrder" SortExpression="REORDER" ReadOnly="true" Visible="false"/>
                        <asp:BoundField DataField="CID" HeaderText="CID" SortExpression="CID" ReadOnly="true" Visible ="false" />
                        <asp:TemplateField ShowHeader="False">
                            <EditItemTemplate>
                                <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-xs" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:Button>
                                &nbsp;<asp:LinkButton ID="btnEdit2" CssClass="btn btn-primary btn-xs" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnEdit2" CssClass="btn btn-primary btn-xs" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" CssClass="btn btn-danger btn-xs" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <asp:SqlDataSource ID="BIDB" runat="server" ConnectionString="<%$ ConnectionStrings:BestInventoryDB %>" DeleteCommand="DELETE FROM [ITEM] WHERE [ID] = @ID" InsertCommand="INSERT INTO [ITEM] ([ITEM_NAME], [DETAIL], [QUT], [COST], [PRICE], [REORDER], [CID]) VALUES (@ITEM_NAME, @DETAIL, @QUT, @COST, @PRICE, @REORDER, @CID)" SelectCommand="SELECT [ID], [ITEM_NAME], [DETAIL], [QUT], [COST], [PRICE], [REORDER], [CID] FROM [ITEM]" UpdateCommand="UPDATE [ITEM] SET [ITEM_NAME] = @ITEM_NAME, [DETAIL] = @DETAIL, [QUT] = @QUT, [COST] = @COST, [PRICE] = @PRICE WHERE [ID] = @ID">
                    <DeleteParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="ITEM_NAME" Type="String" />
                        <asp:Parameter Name="DETAIL" Type="String" />
                        <asp:Parameter Name="QUT" Type="Int32" />
                        <asp:Parameter Name="COST" Type="Decimal" />
                        <asp:Parameter Name="PRICE" Type="Decimal" />
                        <asp:Parameter Name="REORDER" Type="Int32" />
                        <asp:Parameter Name="CID" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ITEM_NAME" Type="String" />
                        <asp:Parameter Name="DETAIL" Type="String" />
                        <asp:Parameter Name="QUT" Type="Int32" />
                        <asp:Parameter Name="COST" Type="Decimal" />
                        <asp:Parameter Name="PRICE" Type="Decimal" />
                        <asp:Parameter Name="REORDER" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </div>

        </div>
        <%--/.col-lg-12--%>


        <div class="col-lg-2">
        </div>
        <%--col-lg-2--%>
    </div>
    <%--/.row--%>
    <%--<div style='display:inline-block; right: 10px;'></div>--%>
</asp:Content>
