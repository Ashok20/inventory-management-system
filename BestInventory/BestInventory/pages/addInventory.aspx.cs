﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BestInventory.pages
{
    public partial class addInventory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["email"] == null) && (Session["pass"] == null))
            {
                Response.Redirect("login.aspx");
            }
        }
        protected void IemAdd_Click(object sender, EventArgs e)
        {
            if (ItemNameAdd.Text!="")
            {
                dbConnection conn = new dbConnection();

                string sql = "INSERT INTO ITEM(ITEM_NAME,DETAIL,QUT,COST,PRICE,REORDER) VALUES('" + ItemNameAdd.Text + "','" + DescriptionAdd.Text + "','" + QuantityAdd.Text + "','" + CostAdd.Text + "','" + PriceAdd.Text + "','" + ReOrderAdd.Text + "')";
                try
                {
                    conn.addDataToDB(sql);
                    ItemNameAdd.Text = "";
                    DescriptionAdd.Text = "";
                    QuantityAdd.Text = "";
                    CostAdd.Text = "";
                    PriceAdd.Text = "";
                    ReOrderAdd.Text = "";
                    ItemNameAdd.Focus();
                }
                catch
                {
                    ItemAddError.Text = "Error Occured When Saving Data";
                }
                
            }
            else
            {
                ItemNameAdd.Focus();
                ItemAddError.Text = "Enter Item Name";
            }
        }

        protected void ClearItem_Click(object sender, EventArgs e)
        {
            ItemNameAdd.Text = "";
            DescriptionAdd.Text = "";
            QuantityAdd.Text = "";
            CostAdd.Text = "";
            PriceAdd.Text = "";
            ReOrderAdd.Text = "";
            ItemNameAdd.Focus();
        }
    }
}