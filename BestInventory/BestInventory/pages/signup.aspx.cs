﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace BestInventory.pages
{
    public partial class signup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignup_Click(object sender, EventArgs e)
        {
            if (fName.Text != "" && lName.Text != "" && email.Text != "" && password.Text != "" && passConfirm.Text != "")
            {
                if (password.Text == passConfirm.Text)
                {
                    dbConnection conn = new dbConnection();

                    String sql2 = "SELECT EMAIL,PASS,UTYPE FROM USERS WHERE EMAIL ='" + email.Text + "'";
                    SqlDataAdapter da = conn.getDataFromDB(sql2);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        SignupErrorTxt.Text = "SignUp EMAIL is already Excist or Empty";
                        fName.Text = "";
                        lName.Text = "";
                        email.Text = "";
                        password.Text = "";
                        passConfirm.Text = "";
                        fName.Focus();
                    }
                    else
                    {
                        string sql = "INSERT INTO USERS(FNAME,LNAME,EMAIL,PASS,UTYPE) VALUES('" + fName.Text + "','" + lName.Text + "','" + email.Text + "','" + password.Text + "','USER')";
                        conn.addDataToDB(sql);

                        Response.Redirect("login.aspx");
                    }
                }
                else
                {
                    SignupErrorTxt.Text = "Passwords Doen Not Match";
                    password.Text = "";
                    passConfirm.Text = "";
                    password.Focus();
                }
            }
            else
            {
                SignupErrorTxt.Text = "Complete the Information";
            }
        }
    }
}
