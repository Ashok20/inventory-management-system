﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/MasterLink.Master" AutoEventWireup="true" CodeBehind="signup.aspx.cs" Inherits="BestInventory.pages.signup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">



    <form id="frmSignup" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign Up</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="fName" runat="server" placeholder="First Name" type="text" autofocus="autofocus"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="lName" runat="server" placeholder="Last Name" type="text"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="email" runat="server" placeholder="Email" type="email"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="password" runat="server" placeholder="Password" type="password"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="passConfirm" runat="server" placeholder="Confirm Password" type="password"></asp:TextBox>
                                </div>

                                <asp:Button ID="btnSignup" class="btn btn-lg btn-success btn-block" runat="server" Text="SignUp" OnClick="btnSignup_Click" />
                                <br />
                                <asp:Label ID="SignupErrorTxt" CssClass="text-danger" runat="server" Text=""></asp:Label>

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>



</asp:Content>
