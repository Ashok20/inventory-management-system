﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/MasterWeb.master" AutoEventWireup="true" CodeBehind="addInventory.aspx.cs" Inherits="BestInventory.pages.addInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="nestedTitle" runat="server">
    Add Inventory
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body1_Heading" runat="server">
    Add Inventory
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body2_Rest" runat="server">

    <div class="row">
        <div class="col-md-8">
            <div class="panel-body">
                <div class="panel panel-default">
                    <fieldset>
                        <div class="panel-heading"></div>

                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="ItemNameAdd" runat="server" placeholder="Item Name" autofocus="autofocus"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="DescriptionAdd" runat="server" placeholder="Item Description"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="QuantityAdd" runat="server" placeholder="Quantity"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="CostAdd" runat="server" placeholder="Cost of Item"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="Password is Required" ControlToValidate="password"></asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="PriceAdd" runat="server" placeholder="Price of Item"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="ReOrderAdd" runat="server" placeholder="Re-Order Level"></asp:TextBox>
                        </div>
                        <%--<div class="form-group col-md-6">
                            <asp:TextBox class="form-control" ID="CategoryAdd" runat="server" placeholder="Category"></asp:TextBox>
                        </div>--%>

                        <div class="col-md-12">
                            <div class="form-group col-md-3">
                                <asp:Button ID="IemAdd" CssClass="btn btn-lg btn-success btn-block btn-sm" runat="server" Text="Add Item" OnClick="IemAdd_Click" />
                            </div>
                            <div class="form-group col-md-3">
                                <asp:Button ID="ClearItem" CssClass="btn btn-lg btn-success btn-block btn-sm" runat="server" Text="Clear" OnClick="ClearItem_Click" />
                            </div>
                            <div class="form-group col-md-3">
                                <asp:Label ID="ItemAddError" CssClass="text-danger" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>

</asp:Content>
