﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/MasterLink.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="BestInventory.pages.login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Login
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="body" runat="server">
    <form id="frmLogin" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="email" runat="server" placeholder="E-mail" type="email" autofocus="autofocus"></asp:TextBox>                           
                                </div>
                                <div class="form-group">
                                    <asp:TextBox class="form-control" ID="password" runat="server" placeholder="Password" type="password"></asp:TextBox>
                                </div>
                                <asp:Button ID="btnLogin" class="btn btn-lg btn-success btn-block" runat="server" Text="Login" OnClick="btnLogin_Click" />
                                <br />
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/pages/signup.aspx">SignUp</asp:HyperLink>
                                <asp:Label ID="LoginErrorTxt" CssClass="text-danger" runat="server" Text=""></asp:Label>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</asp:Content>
