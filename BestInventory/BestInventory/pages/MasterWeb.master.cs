﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace BestInventory.pages
{
    public partial class MasterWeb : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayUserLbl.Text = "Hi, " + Session["username"];
        }
        protected void LogoutLink_Click(object sender, EventArgs e)
        {
            //Logout Button
            Session.Abandon();
            FormsAuthentication.SignOut();
            Session.RemoveAll();
            Response.Redirect("login.aspx");
        }
    }
}